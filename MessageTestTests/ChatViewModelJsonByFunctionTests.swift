//
//  MessageTestTests.swift
//  MessageTestTests
//
//  Created by Zhan, C. (Can) on 8/3/18.
//  Copyright © 2018 Zhan, C. (Can). All rights reserved.
//

import XCTest
@testable import MessageTest

class ChatViewModelJsonByFunctionTests: XCTestCase {
    
    let viewModel = ChatViewModel()
    
    func testEmptyCase() {
        let message = ""
        let minifiedExpectedJson = "{}"
        
        let expectation = self.expectation(description: "Generating JSON")
        viewModel.jsonBy(detecting: message) { (json) in
            let minifiedJson = json.replacingOccurrences(of: " ", with: "")
            XCTAssert(minifiedJson == minifiedExpectedJson)
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error.localizedDescription)")
            }
        }
    }
    
    func testNormalMessageCase() {
        let message = "This is a normal message"
        let minifiedExpectedJson = "{}"
        
        let expectation = self.expectation(description: "Generating JSON")
        viewModel.jsonBy(detecting: message) { (json) in
            let minifiedJson = json.replacingOccurrences(of: " ", with: "")
            XCTAssert(minifiedJson == minifiedExpectedJson)
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error.localizedDescription)")
            }
        }
    }
    
    func testMentionMessageCase() {
        let message = "@chris you around?"
        let minifiedExpectedJson = "{\"mentions\":[\"chris\"]}"
        
        let expectation = self.expectation(description: "Generating JSON")
        viewModel.jsonBy(detecting: message) { (json) in
            let minifiedJson = json.replacingOccurrences(of: " ", with: "")
            XCTAssert(minifiedJson == minifiedExpectedJson)
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error.localizedDescription)")
            }
        }
    }
    
    func testEmoticonMessageCase() {
        let message = "Good morning! (megusta) (coffee)"
        let minifiedExpectedJson = "{\"emoticons\":[\"megusta\",\"coffee\"]}"
        
        let expectation = self.expectation(description: "Generating JSON")
        viewModel.jsonBy(detecting: message) { (json) in
            let minifiedJson = json.replacingOccurrences(of: " ", with: "")
            XCTAssert(minifiedJson == minifiedExpectedJson)
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error.localizedDescription)")
            }
        }
    }
    
    func testLinkMessageCase() {
        let message = "Olympics are starting soon;http://www.nbcolympics.com"
        
        let expectation = self.expectation(description: "Generating JSON")
        viewModel.jsonBy(detecting: message) { (json) in
            guard let dictionary = self.dictionary(of: json),
                let linkString = (dictionary["links"] as? [Any])?[0] as? String,
                let linkDictionary = self.dictionary(of: linkString),
                let url = linkDictionary["url"] as? String,
                let title = linkDictionary["title"] as? String else {
                XCTFail("No valid JSON generated")
                return
            }
            
            XCTAssertEqual(url, "http://www.nbcolympics.com")
            XCTAssertEqual(title, "2018 PyeongChang Olympic Games | NBC Olympics")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error.localizedDescription)")
            }
        }
    }
    
    func testMixedMessageCase() {
        let message = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
        
        let expectation = self.expectation(description: "Generating JSON")
        viewModel.jsonBy(detecting: message) { (json) in
            guard let dictionary = self.dictionary(of: json),
                let mentions = dictionary["mentions"] as? [String],
                let emoticons = dictionary["emoticons"] as? [String],
                let linkString = (dictionary["links"] as? [Any])?[0] as? String,
                let linkDictionary = self.dictionary(of: linkString),
                let url = linkDictionary["url"] as? String,
                let title = linkDictionary["title"] as? String else {
                    XCTFail("No valid JSON generated")
                    return
            }
            
            XCTAssert(mentions.contains("bob"))
            XCTAssert(mentions.contains("john"))
            XCTAssert(emoticons.contains("success"))
            XCTAssertEqual(url, "https://twitter.com/jdorfman/status/430511497475670016")
            XCTAssertEqual(title.replacingOccurrences(of: "&quot;", with: "\""), "Justin Dorfman; on Twitter: \"nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq\"")
            
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 30) { (error) in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error.localizedDescription)")
            }
        }
    }
    
    private func dictionary(of jsonString: String) -> Dictionary<String, Any>? {
        let data = jsonString.data(using: .utf8)!
        do {
            if let dict = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String, Any> {
                return dict
            }
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
    
    
}
