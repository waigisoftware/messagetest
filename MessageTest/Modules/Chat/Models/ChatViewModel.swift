//
//  ChatViewModel.swift
//  MessageTest
//
//  Created by Zhan, C. (Can) on 13/3/18.
//  Copyright © 2018 Zhan, C. (Can). All rights reserved.
//

import Foundation

class ChatViewModel {
    
    // MARK: - Model data
    
    /// Group original chat message and converted JSON string together
    struct ChatMessage {
        let message: String
        let json: String
    }
    
    struct CellIdentifiers {
        static let message = "message"
        static let json = "json"
    }
    
    /// Cell identifiers of all cells to be rendered
    var cellIdentifiers: [String] {
        return Array(repeating: [CellIdentifiers.message, CellIdentifiers.json], count: messages.count).flatMap{ $0 }
    }
    
    /// Chat messages history list
    var messages = [ChatMessage]()
    
    // MARK: - Utils to parse message string
    
    /// The special content types to be detected
    enum SpecialContentType {
        case mention
        case emoticon
        case link
        
        /// All enums of SpecialContentType
        static var all: [SpecialContentType] {
            return [.mention, .emoticon, .link]
        }
        
        /// JSON key value of each type
        var jsonKey: String {
            switch self {
            case .mention: return "mentions"
            case .emoticon: return "emoticons"
            case .link: return "links"
            }
        }
        
        /// Regular Expression pattern
        var pattern: String {
            switch self {
            case .mention: return "(\\@\\w+)"       // detect all words with @ as prefix
            case .emoticon: return "\\((\\w+)\\)"   // detect all words which are wrapped by parenthesis
            case .link: return ""                   // detect links
            }
        }
    }
    
    private typealias SpecialContentTypeWithMatches = (type: SpecialContentType, matches: [Substring])
    
    /// Do specified regular expression match on given string
    private func matches(in string: String, types: [SpecialContentType]) -> [SpecialContentTypeWithMatches] {
        let matches: [SpecialContentTypeWithMatches] = types.flatMap {
            let checkingResults: [NSTextCheckingResult]
            
            switch $0 {
            case .emoticon,
                 .mention:
                let regularExpression = try! NSRegularExpression(pattern: $0.pattern, options: .dotMatchesLineSeparators)
                checkingResults = regularExpression.matches(in: string, options: [], range: NSRange(location: 0, length: string.utf16.count))
            case .link:
                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                checkingResults = detector.matches(in: string, options: [], range: NSRange(location: 0, length: string.utf16.count))
            }
            
            let contents: [Substring] = checkingResults.flatMap {
                let substring: Substring?
                if let range = Range($0.range, in: string) {
                    substring = string[range]
                } else {
                    substring = nil
                }
                return substring
            }
            
            return !contents.isEmpty ? SpecialContentTypeWithMatches(type: $0, matches: contents) : nil
        }
        
        return matches
    }
    
    /// Abstract contents of each regex matching results. e.g. ["@Can"] -> ["Can"], ["(Smile)"] -> ["Smile"]
    private func contents(from typeWithMatch: SpecialContentTypeWithMatches) -> [String] {
        switch typeWithMatch.type {
        case .mention: return typeWithMatch.matches.map { String($0.dropFirst()) }
        case .emoticon: return typeWithMatch.matches.map { String($0.dropFirst().dropLast()) }
        case .link: return typeWithMatch.matches.map { String($0) }
        }
    }
    
    /// Concatenate all matches into a JSON string
    private func jsonString(from typeWithMatches: [SpecialContentTypeWithMatches], with urlAndTitleJson: [String]? = nil) -> String {
        var json = "{"
        let content = typeWithMatches.flatMap {
            if $0.type == .link,
                let urlAndTitleJson = urlAndTitleJson {
                return "\"\($0.type.jsonKey)\": \(urlAndTitleJson)"
            } else {
                return "\"\($0.type.jsonKey)\": \(String(describing: contents(from: $0)))"
            }
            }
            .joined(separator: ", ")
        json += " \(content) }"
        return json
    }
    
    /// Detect <Title></Title> section from given HTML string. nil is returned if not found.
    private func title(of htmlString: String) -> String? {
        let titlePattern = "(\\<title\\>).+(\\<\\/title\\>)"
        let regularExpression = try! NSRegularExpression(pattern: titlePattern, options: .caseInsensitive)
        let checkingResults = regularExpression.matches(in: htmlString, options: [], range: NSRange(location: 0, length: htmlString.utf16.count))
        
        let contents: [Substring] = checkingResults.flatMap {
            let substring: Substring?
            if let range = Range($0.range, in: htmlString) {
                substring = htmlString[range].dropFirst("<title>".count).dropLast("</title>".count)
            } else {
                substring = nil
            }
            return substring
        }
        
        if let substring = contents.first {
            return String(substring)
        } else {
            return nil
        }
    }
    
    /// Fetch page title of all Links asynchronously and call the completion closure when all fetches are done. Constructed JSONs is available in the handler.
    private func loadTitlesWith(urlStrings: [String], completion: @escaping ([String]) -> Void) {
        DispatchQueue.global().async {
            var jsons = [String]()
            let fetchingTitleGroup = DispatchGroup()
            // asynchronously load titles
            urlStrings.forEach { urlString in
                if let url = URL(string: String(urlString)) {
                    fetchingTitleGroup.enter()
                    URLSession.shared.dataTask(
                        with: url,
                        completionHandler: { [weak self] (data, response, error) in
                            guard let strongSelf = self else { return }
                            var fetchedTitle = ""
                            if let error = error {
                                fetchedTitle = "Failed to get title from url: \(urlString) with error: \(error)"
                            } else if let data = data,
                                let htmlString = String(data: data, encoding: String.Encoding.utf8),
                                let titleString = strongSelf.title(of: htmlString) {
                                fetchedTitle = titleString
                            }
                            let json = "{ \"url\": \"\(urlString)\", \"title\": \"\(fetchedTitle)\" }"
                            jsons.append(json)
                            fetchingTitleGroup.leave()
                    })
                        .resume()
                }
            }
            
            // call back when all fetches are done
            fetchingTitleGroup.wait()
            DispatchQueue.main.async {
                completion(jsons)
            }
        }
    }
    
    /// Detect a given message string and return detection result as a JSON string
    func jsonBy(detecting message: String, of typesToDetect: [SpecialContentType] = SpecialContentType.all, isFetchingLinkTitle: Bool = true, completion: @escaping (String) -> Void) {
        let typeWithMatches = matches(in: message, types: typesToDetect)
        
        // fetch link titles online
        if isFetchingLinkTitle {
            let urls = typeWithMatches.filter { $0.type == .link }.flatMap { $0.matches }.map { String($0) }
            loadTitlesWith(
                urlStrings: urls,
                completion: { [weak self] jsons in
                    guard let strongSelf = self else { return }
                    // construct json when all done
                    completion(strongSelf.jsonString(from: typeWithMatches, with: jsons))
            })
            
            // do not need to fetch titles
        } else {
            completion(jsonString(from: typeWithMatches))
        }
    }
    
}
