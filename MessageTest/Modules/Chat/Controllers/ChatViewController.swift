//
//  ChatViewController.swift
//  MessageTest
//
//  Created by Zhan, C. (Can) on 13/3/18.
//  Copyright © 2018 Zhan, C. (Can). All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var tapGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet weak var inputTextField: UITextField!
    
    let viewModel = ChatViewModel()
    
    // MARK: - Initial setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tapGestureRecognizer.addTarget(self, action: #selector(dismissKeyboard))
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        deregisterForKeyboardNotifications()
    }
    
}

// MARK: - Table view data source and delegate
extension ChatViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cellIdentifiers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifiers[indexPath.row], for: indexPath)
        let chatMessage = viewModel.messages[indexPath.row / 2]
        
        if let messageCell = cell as? MessageTableViewCell {
            messageCell.messageLabel.text = chatMessage.message
        } else if let jsonCell = cell as? JSONTableViewCell {
            jsonCell.jsonLabel.text = chatMessage.json.replacingOccurrences(of: "&quot;", with: "\"")
        }
        
        return cell
    }
}

// MARK: - UITextFieldDelegate
extension ChatViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let inputString = textField.text else { return true }
        // detect message and render on screen
        viewModel.jsonBy(detecting: inputString) { [weak self] (json) in
            guard let strongSelf = self else { return }
            strongSelf.viewModel.messages.append(ChatViewModel.ChatMessage(message: inputString, json: json))
            DispatchQueue.main.async {
                strongSelf.tableView.reloadData()
                strongSelf.tableView.scrollToRow(at: IndexPath(row: strongSelf.viewModel.messages.count * 2 - 1, section: 0),
                                                 at: .bottom,
                                                 animated: true)
            }
        }
        textField.text = nil
        return true
    }
}

// MARK: Handle keyboard

extension ChatViewController {
    fileprivate func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(_:)), name: Notification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(_:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func deregisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc fileprivate func keyboardWasShown(_ aNotification: Notification) {
        guard let info = aNotification.userInfo else { return }
        
        let kbSize = (info[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue.size
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc fileprivate func keyboardWillBeHidden(_ aNotification: Notification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
