//
//  MessageTableViewCell.swift
//  MessageTest
//
//  Created by Zhan, C. (Can) on 13/3/18.
//  Copyright © 2018 Zhan, C. (Can). All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    
}
