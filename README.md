## Run the Project 

1. Run as an App. Just clone and run the project with simulator. While the App launches, focus on the input field down to the bottom, typing any messages and observe generated JSON string above. Please note, if you run the app with iOS 11 simulator, please use https instead of http to see links.
2. Run as unit test cases. Find ChatViewModelJsonByFunctionTests.swift, run all test cases by clicking the run button left to the class.

---

## Project Structure

1. MessageTest contains Modules folder, inside which has only one subfolder/module -- Chat at this test.
2. As the subfolder names tell, the architecture is MVVM. All message handling logics resides in ChatViewModel.
3. The function 
```
jsonBy(detecting message: String, of typesToDetect: [SpecialContentType] = SpecialContentType.all, isFetchingLinkTitle: Bool = true, completion: @escaping (String) -> Void).
```
is the entry point of detecting a message
